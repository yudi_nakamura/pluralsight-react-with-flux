import React from "react"
import { Link } from "react-router-dom"
import PropTypes from "prop-types";

const CoursesList = (props) => {
  return (
    <table className="table">
      <thead>
        <tr>
          <th>Title</th>
          <th>Author ID</th>
          <th>Category</th>
          <th>Delete</th>
        </tr>
      </thead>
      <tbody>
        {props.courses.map(course => {
          return (
            <tr key={course.id}>
              <td><Link to={`/course/${course.slug}`}>{course.title}</Link></td>
              <td>{course.authorId}</td>
              <td>{course.category}</td>
              <td><button type="button" className="btn btn-danger" onClick={() => props.onDelete(course.id)}>X</button></td>
            </tr>
          )
        })}
      </tbody>
    </table>
  )
}

CoursesList.prototype = {
  courses: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      title: PropTypes.string.isRequired,
      authorId: PropTypes.number.isRequired,
      category: PropTypes.string.isRequired
    })
  ).isRequired,
  onDelete: PropTypes.func.isRequired
}

export default CoursesList
