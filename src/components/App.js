import React from "react"
import { Route, Switch, Redirect } from "react-router-dom"
import { ToastContainer } from 'react-toastify'

// * * * * * Page Components * * * * *
import HomePage from "./HomePage"
import AboutPage from "./AboutPage"
import CoursesPage from "./CoursesPage"
import NotFoundPage from "./NotFoundPage"
import ManageCoursePage from "./ManageCoursePage"
// * * * * * Common Components * * * * *
import Header from "./common/Header"
// * * * * * Redirect URLs * * * * *
import redirectURLs from "../redirectURLs"

const App = () => {
  return (
    <div className="container-fluid">
      <ToastContainer autoClose={3000} hideProgressBar/>
      <Header />
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route path="/courses" component={CoursesPage} />
        <Route path="/about" component={AboutPage} />
        <Route path="/course/:slug" component={ManageCoursePage} />
        <Route path="/course" component={ManageCoursePage} />
        {redirectURLs.map(url => {
          return <Redirect key={url.from} from={url.from} to={url.to} />
        })}
        <Route component={NotFoundPage} />
      </Switch>
    </div>
  )
}

export default App
