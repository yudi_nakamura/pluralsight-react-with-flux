import React, { useState, useEffect } from "react"
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

// * * * * * Components * * * * *
import CourseForm from "./CourseForm"
// * * * * * Store * * * * *
import store from "../stores/courseStore"
import * as courseActions from "../actions/courseActions"

const ManageCoursePage = (props) => {
  const [errors, setErrors]   = useState({})
  const [courses, setCourses] = useState(store.getCourses())
  const [course, setCourse]   = useState({
    id      : null,
    slug    : '',
    title   : '',
    authorId: null,
    category:  ''
  })

  function onChange() {
    setCourses(store.getCourses())
  }

  useEffect(() => {
    store.addChangeListener(onChange)
    const slug = props.match.params.slug
    if (courses.length === 0) {
      courseActions.loadCourses()
    } else if (slug) {
      setCourse(store.getCourseBySlug(slug))
    }
    return () => store.removeChangeListener(onChange)
  }, [courses.length, props.match.params.slug])

  function handleChange({target}) {
    setCourse({...course, [target.name]: target.value})
  }

  function formIsValid() {
    const _erros = {}

    if (!course.title) _erros.title       = "Title is required"
    if (!course.authorId) _erros.authorId = "Author is required"
    if (!course.category) _erros.category = "Category is required"

    setErrors(_erros)

    return Object.keys(_erros).length === 0
  }

  function handleSubmit(event) {
    event.preventDefault()

    if (!formIsValid()) return

    courseActions
      .saveCourse(course)
      .then(() => {
        props.history.push('/courses')
        toast.success('Course saved!')
      })
  }

  return (
    <>
      <h2>Manage Course</h2>
      <CourseForm course={course} errors={errors} onChange={handleChange} onSubmit={handleSubmit}/>
    </>
  )
}

export default ManageCoursePage
