import React from "react"

class AboutPage extends React.Component {
  render() {
    return (
      <>
        <h2>About</h2>
        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Fugit, earum cum. Accusamus iure magni alias quaerat quo magnam! Nobis, non?</p>
      </>
    )
  }
}

export default AboutPage
