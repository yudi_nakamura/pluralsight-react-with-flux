import React, { useEffect, useState } from "react"
import { Link } from "react-router-dom"
import { toast } from 'react-toastify'

// * * * * * Components * * * * *
import CoursesList from "./CoursesList"
// * * * * * Store * * * * *
import store from "../stores/courseStore"
import { loadCourses, deleteCourse } from "../actions/courseActions";

const CoursesPage = () => {
  const [courses, setCourses] = useState(store.getCourses())

  useEffect(() => {
    store.addChangeListener(onChange)
    if (courses.length === 0) loadCourses()
    return () => store.removeChangeListener(onChange)
  },[courses.length])

  function onChange() {
    setCourses(store.getCourses())
  }

  function handleDelete(courseID) {
    deleteCourse(courseID).then(() => toast.success('Course deleted!'))
  }

  return (
    <React.Fragment>
      <h2>Couses Page</h2>
      <Link to="/course" className="btn btn-primary mb-3">Add Course</Link>
      <CoursesList courses={courses} onDelete={handleDelete}/>
    </React.Fragment>
  )
}

export default CoursesPage
