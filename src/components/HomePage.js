import React from "react"
import { Link } from "react-router-dom";

const HomePage = () => {
  return (
    <div className="jumbotron">
      <h1>Admin</h1>
      <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste, accusantium!</p>
      <Link to="about" className="btn btn-primary">About</Link>
    </div>
  )
}

export default HomePage
