import React from "react"
import { NavLink } from "react-router-dom";

const Header = () => {
  const activeStyle = {color: "orange"}
  return (
    <nav>
      <NavLink exact to="/" activeClassName="active" activeStyle={activeStyle}>Home</NavLink>{" | "}
      <NavLink to="/courses" activeClassName="active" activeStyle={activeStyle}>Course</NavLink>{" | "}
      <NavLink to="/about" activeClassName="active" activeStyle={activeStyle}>About</NavLink>
    </nav>
  )
}

export default Header
