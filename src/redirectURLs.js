const redirectURLs = [
  {'from': '/about-page', 'to': '/about'},
  {'from': '/courses-page', 'to': '/courses'}
]

export default redirectURLs
